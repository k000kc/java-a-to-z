package ru.apetrov.generic;

/**
 * Created by Andrey on 08.03.2017.
 */
public class RoleStore extends AbstractStore<Role> {

    /**
     * Constructor of role store.
     * @param size size store.
     */
    public RoleStore(int size) {
        super(size);
    }
}
